package ch.bbw.chatBot;

import java.util.HashMap;
import java.util.HashSet;

/**
 * The responder class represents a response generator object. It is
 * used to generate an automatic response.
 * 
 * @author     Michael Kolling and David J. Barnes
 * @version    0.1  (1.Feb.2002)
 */
public class Responder {
	HashMap<String, String> responseMap = new HashMap<>();
	/**
	 * Construct a Responder - nothing to do
	 */
	public Responder() {
		
		responseMap.put("slow",
				"I think this has to do with your hardware. \n" +
						"Upgrading your processor should solve all \n" +
						"performance problems. Have you got a problem with \n" +
						"our software?");
		responseMap.put("bug",
				"What kind of bug are you experiencing exactly?");
		responseMap.put("weird", "What kind of weird behaviour are you experiencing?");
		responseMap.put("crash", "What did you do when it crashed? If possible, can you\n" +
				" replicate it and send the error code that you got? Please send your\n" +
				" error code like that: \"error code: your error code \"");
		responseMap.put("crashing", "What did you do when it crashed? If possible, can you\n" +
				" replicate it and send the error code that you got? Please send your\n" +
				" error code like that: \"error code: your error code \"");
		responseMap.put("crashes", "What did you do when it crashed? If possible, can you\n" +
				" replicate it and send the error code that you got? Please send your\n" +
				" error code like that: \"error code: your error code \"");
		responseMap.put("error code:", "Thank you for reporting your issue!\n" +
				" Our team will work on it and resolve it");
		responseMap.put("hi", "Welcome to our support system. I'm an intelligent\n" +
				"Support bot. Please tell me your problem");
	}

	/**
	 * Generate a response.
	 * @return   A string that should be displayed as the response
	 */
	public String generateResponse(HashSet<String> words) {
		for (String keyword : words) {
			String response = responseMap.get(keyword);
			if(response != null) {
				return response; // exit Method
			}
		}
		return this.pickDefaultResponse(); // default responses
	}
	
	private String pickDefaultResponse() {
		return "This sounds interesting, tell us a bit more please";
	}
}
