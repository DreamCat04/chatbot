package ch.bbw.diverses;

import ch.bbw.chatBot.InputReader;
import ch.bbw.chatBot.Responder;
import ch.bbw.chatBot.SupportSystem;

import java.util.HashMap;

public class HashMapTest {
	public HashMapTest() {
		//       Key     Value
		HashMap<String, String> phoneBook = new HashMap<>();
		
		phoneBook.put("Peter", "(044) 347 8080");
		phoneBook.put("Andrea", "(044) 345 6666");
		phoneBook.put("Willi", "(052) 222 0123");

	
		// Aufgaben 1.2
		// ----------------------------------------------
		// Aufgabe a)
		// ...

		// Aufgabe b)
		// ...
		
		// Aufgabe c)
		// ...
		
		// Aufgabe c) Version 2 mit EntrySet
		// ...
		
		
		// Aufgabe 1.3 mit Person Klasse
		// Estellen Sie die Klasse Person wie gefordert und wiederholen Sie die Aufgaben a) bis c)
		// ----------------------------------------------
//		HashMap<String, Person> betterPhonebook = new HashMap<>();
		
		// Aufgabe a)
		// ...

		// Aufgabe b)
		// ...
		
		// Aufgabe c)
		// ...
		
		// Aufgabe c) Version 2 mit EntrySet
		// ...
}
}

